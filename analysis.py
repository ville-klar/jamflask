import numpy as np
import pandas as pd

def mean_values_as_json(df):
    mean_df = df.groupby("type").mean()
    jsonOut = mean_df.to_json(orient="index")
    return jsonOut
