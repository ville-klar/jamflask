# JAMFlask

> This is minimal example to introduce the idea. It might work for prototypes but a full-fledged framework (e.g. [Django](https://www.djangoproject.com/))
> is probably a more sustainable route for development of proper analysis web apps.

This example demonstrates an *analysis-api* that takes a csv-file as input and outputs results of a analysis (written in python) as json.
The resulting json is used to generate a barchart and table that are rendered client-side (javascript).
The barchart is generated with [ChartJS](https://www.chartjs.org/).
The frontend is vanilla-JS to keep it simple.

This approach / stack is called : [jamstack.org](https://jamstack.org/)

# HOW TO

Install the requirements, use a virtualenv if you want

```
pip install -r  requirements
```

Run the app

```
python app.py
```

You can access it at [localhost:5000](http://localhost:5000).


# TODO  

- [ ] Add docker files (and instructions)
- [ ] Use caddy to serve the static files and http proxy to api
- [ ] Deployment with gunicorn
- [ ] Write more documentation (especially on the js)
