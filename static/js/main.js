function testPlot() {
    var req = new XMLHttpRequest();
    req.responseType = "json";
    req.onreadystatechange = function() {
        if (req.readyState == 4 && req.status == 200)
            visualizeJSON(req.response);
    }
    req.open("GET", 'api', true); // true for asynchronous
    req.send(null);
}

function postCSV(obj) {
    let formData = new FormData();
    let req = new XMLHttpRequest();
    req.responseType = "json";
    let csvFile = obj.files[0];  // file from input
    formData.append("data-file", csvFile);
    req.onreadystatechange = function() {
        if (req.readyState === 4 && req.status == 200) {
            visualizeJSON(req.response);
        }
    }
    req.open("POST", 'api');
    req.send(formData);
}   

function extractData (data) {
    var groups = Object.keys(data);
    var variables = Object.keys(data[groups[0]]);       //We assume these are the same for every group
    var values = Create2DArray(variables.length);       //create 2d array for data values
    for (var j in groups){   
        var x = data[groups[j]];
        for (var i in variables){     
            values[i][j] = x[variables[i]];
        }
    }
    return [values, groups, variables];
}

function Create2DArray(rows) {
  var arr = [];
  for (var i=0;i<rows;i++) {
     arr[i] = [];
  }
  return arr;
}

function createTable(tableData, colnames, rownames) {
    var table = document.createElement('table');
    var row = {};
    var cell = {};
    tableData.forEach(function(rowData,i) {
        row = table.insertRow(-1);
        cell = row.insertCell();
        cell.innerHTML = "<b>"+rownames[i]+"</b>";
        rowData.forEach(function(cellData) {
            cell = row.insertCell();
            cell.textContent = cellData;
        });
    });
    var header = table.createTHead();
    var headerRow = header.insertRow(0);
    cell = headerRow.insertCell();
    cell.innerHTML = "";
    colnames.forEach(function(name){
        cell = headerRow.insertCell();
        cell.innerHTML = "<b>"+name+"</b>";
    });
    var tableLoc = document.getElementById("tableOut")
    if (tableLoc.hasChildNodes()) {
        tableLoc.removeChild(tableLoc.childNodes[0]);
        }
    tableLoc.appendChild(table);
}


function createBarChart(values, groups, variables){
    var barData = {
        labels: groups,
        datasets: [{
            label: variables[0],
            backgroundColor: 'rgba(255, 99, 132, 0.3)',
            borderColor: 'rgb(255, 99, 132)',
            borderWidth: 1,
            data: values[0]
        },{
            label: variables[1],
            backgroundColor: 'rgba(55, 199, 232, 0.3)',
            borderColor: 'rgb(55, 199, 232)',
            borderWidth: 1,
            data: values[1]
        },{
            label: variables[2],
            backgroundColor: 'rgba(5, 233, 32, 0.3)',
            borderColor: 'rgb(5, 233, 32)',
            borderWidth: 1,
            data: values[2]
        },{
            label: variables[3],
            backgroundColor: 'rgba(255, 233, 32, 0.3)',
            borderColor: 'rgb(255, 233, 32)',
            borderWidth: 1,
            data: values[3]
        }]
    };

    var ctx = document.getElementById('chart0');
    if(window.myBar!=null){
        myBar.destroy();
    }
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barData,
        options: {
            elements: {
                rectangle: {
                    borderWidth: 2,
                }
            },
            responsive: true,
            legend: {
                position: 'right',
            },
            title: {
                display: true,
                text: 'Plot Title'
            }
        }
    });
}

function visualizeJSON(data){
    [values,groups,variables] = extractData(data);
    createBarChart(values, groups, variables)
    createTable(values, groups, variables);
    document.getElementById("jsonOut").innerHTML = JSON.stringify(data, null, 2);
}


window.onload = function() {
    var canvas = document.getElementById('chart0');
    var ctx = canvas.getContext("2d");
    ctx.font = "20px Arial";
    ctx.fillText("Waiting for data set", 40, 50);
    document.getElementById('testPlot').addEventListener('click', function() {testPlot();});
};
