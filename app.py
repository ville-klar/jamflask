#!flask/bin/python
from flask import Flask, jsonify, request
import pandas as pd
import analysis

app = Flask(__name__,static_url_path='')

ALLOWED_EXTENSIONS = set(['csv'])

def check_file(filename):
   return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS 

@app.route('/')
def static_file():
    return app.send_static_file('index.html')

@app.route('/api', methods=['GET','POST'])
def get_analysis():
    if 'data-file' not in request.files:
        file = 'sampleData/iris.csv'
        df = pd.read_csv(file)
        return analysis.mean_values_as_json(df)
    print("received data file")
    file = request.files['data-file']
    if file and check_file(file.filename):
        df = pd.read_csv(file)
        return analysis.mean_values_as_json(df)
    else:
        return ""

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
    # app.run(debug=True)
